package com.demo.calendar;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/*

 @Author melone
 @Date 6/24/18 
 
 */
public class MonthlyCalendar {
    //the CALENDAR_SIZE is set to 35 i.e. 5 rows and 7 solumns excluding the day name
    private final int CALENDAR_SIZE = 35;
    //Here array of days in a week is defined in daysinMonth array of Strings
    String daysinMonth[] = {"sun", "mon", "tue", "wed", "thu", "fri"};
    Logger logger = Logger.getLogger("MonthlyCalendar.class");
    private MangsirMonth mangsir;

    //the MangsirMonth bean will be injected through xml
    public void setMangsir(MangsirMonth mangsir) {
        this.mangsir = mangsir;
    }

    /**
     * printCalendar is invoked from main class i.e CalendarDemo
     *
     * @see CalendarDemo
     * This method firstly stores days in an days array of int
     * Then it prints days of month by considerint gap provided
     * gap defines the starting of day 1 in calendar
     * if gap is 4 day 1 will be thursday
     */
    public void printCalendar() {
        final int NO_OF_DAYS = mangsir.getDays();
        final int DAY_GAP = mangsir.getGap();
        //size of array of dates is defined in date[]
        int date[] = new int[NO_OF_DAYS + DAY_GAP];
        //variable l is used to store the days at the top of array if arraysize exceed than calendar size
        int l = -1;
        for (int i = 0; i < date.length; i++) {
            //fill the array with 0 for defined gap
            if (i < DAY_GAP) {
                date[i] = 0;
            } else {
                //store day at the top of list if arraysize exceed to CALENDAR_SIZE
                if (i > CALENDAR_SIZE - 1) {
                    date[++l] = i - DAY_GAP + 1;
                }
                //store value of day in array normally
                else {
                    date[i] = i - DAY_GAP + 1;
                }
            }
        }
        System.out.println(String.format("%-20s","\t\t\tMansir 2075"));
        //print the days of week using java8 stream
        Arrays.stream(daysinMonth).forEach(day -> {
            System.out.print(String.format("%-5s", day));
        });
        try {
            Thread.sleep(100);
            System.err.print(String.format("Sat"));
            Thread.sleep(100);
            //print new line after printing days
            System.out.println();
        }catch (InterruptedException ex){
            logger.log(Level.INFO, ex.getMessage());
        }
       // System.out.println();
        //print the calendar
        for (int i = 0; i < date.length; i++) {
            //replace 0 by space gap to console
            if (date[i] == 0) {
                System.out.print(String.format("%-5s", ""));
            } else {
                //check whether to add new line or not in calendar
                if ((i + 1) % 7 == 0) {
                    try {
                        //thread is used to get sequential result from System.out and System.err
                        //otherwise System.out and System.err is printed in random order
                        Thread.sleep(100);
                        //print date of saturday in red color
                        System.err.print(String.format("%-5s", date[i]));
                        //sleep thread for 100 miliseconds
                        Thread.sleep(100);
                        //add new line after saturday
                        System.out.println();
                    } catch (InterruptedException ex) {
                        logger.log(Level.INFO, ex.getMessage());
                    }
                }
                //if day is not the saturday print in normal order
                else {
                    System.out.print(String.format("%-5s", date[i]));
                }
            }
        }
    }
}
