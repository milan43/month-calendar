package com.demo.calendar;

/*

 @Author melone
 @Date 6/24/18 
 
 */
public class MangsirMonth {
    private int days;
    private int gap;

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getGap() {
        return gap;
    }

    public void setGap(int gap) {
        this.gap = gap;
    }
}
