package com.demo.calendar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/*

 @Author melone
 @Date 6/24/18 
 
 */
public class CalendarDemo {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("calendar-config.xml");
        MonthlyCalendar calendar = ((MonthlyCalendar) context.getBean("calendar"));
        calendar.printCalendar();
    }
}
